package com.yedam.java.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectExample {

	public static void main(String[] args) {
		try {
		// 1. JDBC Driver 로딩하기
		Class.forName("org.sqlite.JDBC");
		
		// 2. DB 서버 접속하기
		String url = "jdbc:sqlite:/D:/dev/database/TestDataBase.db";
		Connection conn =DriverManager.getConnection(url);
		//절대 경로로 해야하한다.
		// /로 경로를 지정해야한다.
		
		// 3. Statement or PreparedStatement 객체 생성하기
		Statement stmt = conn.createStatement();
		
		// 4. SQL 실행
		ResultSet rs = stmt.executeQuery("SELECT student_id, student_name, student_dept FROM Students");
		
		// 5. 결과값 출력하기
		while(rs.next()) {
			int sId = rs.getInt("student_id");
			String sName = rs.getString("student_name");
			String sDept = rs.getString("student_dept");
			System.out.println("학번 : " + sId);
			System.out.println("이름 : " + sName);
			System.out.println("학년 : " + sDept);
		}
		}catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
		// 6. 자원해제하기
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
			if(conn != null) conn.close();
		}
		
		

	}

}
